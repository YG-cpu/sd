#include <stdio.h>

#include "colours.h"

void die(char* err_msg)
{
  printf(red"%s", err_msg);
}
